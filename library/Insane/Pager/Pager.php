<?php
namespace Insane\Pager;

class Pager {
    private $page;
	private $objectCount;
	private $objectsPerPage;
	private $pageCount;
	private $maxPages;
	private $urlTemplate = '<a href="?p=%s">%s</a>';
	
	/**
	 * @param integer $page The current page
	 * @param integer $objectCount The total number of objects
	 * @param integer $objectsPerPage The number of objects per page
	 */
	public function __construct($page = 1, $objectCount = null, $objectsPerPage = null) {
	    $this->page = $page;
	    $this->setObjectCount($objectCount);
	    $this->setObjectsPerPage($objectsPerPage);
	}
	
	/**
	 * Set the total number of objects
	 * @param integer $objectCount
	 * @return \Insane\Pager\Pager
	 */
	public function setObjectCount($objectCount) {
	    $this->objectCount = $objectCount;
	    $this->updatePageCount();
	    return $this;
	}
	
	/**
	 * Set the number of objects per page
	 * @param integer $objectsPerPage
	 * @return \Insane\Pager\Pager
	 */
	public function setObjectsPerPage($objectsPerPage) {
	    $this->objectsPerPage = $objectsPerPage;
	    $this->updatePageCount();
	    return $this;
	}
	
	/**
	 * Sets the maximum number of pages to show. This doesn't include prev and next links
	 * @param integer $maxPages
	 * @return \Insane\Pager\Pager
	 */
	public function setMaxPages($maxPages) {
	    $this->maxPages = $maxPages;
	    return $this;
	}
	
	/**
	 * Set the url template as a formatted string, i.e. <a href="?p=%s">%s</a>
	 * The first argument is the page and the second argument is the anchor text
	 * @param string $urlTemplate
	 */
	public function setUrlTemplate($urlTemplate) {
	    $this->urlTemplate = $urlTemplate;
	    return $this;
	}
	
	private function updatePageCount() {
	    if(!$this->objectCount || !$this->objectsPerPage) {
	        $this->pageCount = 0;
	        return;
	    }
	    $this->pageCount = ceil($this->objectCount / $this->objectsPerPage); 
	}
	
	/**
	 * @return integer The total number of pages
	 */
	public function getPageCount() {
	    return $this->pageCount;
	}
	
	public function getHtml() {
	    $str = '';
	    $str .= '<ul class="pager">';
	    $str .= '<li' . ($this->page < 2 ? ' class="disabled"' : '') . '>' . sprintf($this->urlTemplate, $this->page-1, '&laquo;') . '</li>';
	    
	    if($this->maxPages && $this->pageCount > ($this->maxPages + 5)) {
	        if($this->page < 1 + $this->maxPages) {
	            for($i = 1; $i < $this->maxPages + 4; $i++) {
	                $str .= '<li' . ($this->page == $i ? ' class="active"' : '') . '>' . sprintf($this->urlTemplate, $i, $i) . '</li>';
	            }
	            $str .= '<li class="disabled"><a href="javascript:void(0)">...</a></li>';
	            $str .= '<li>' . sprintf($this->urlTemplate, $this->pageCount - 1, $this->pageCount - 1) . '</li>';
	            $str .= '<li>' . sprintf($this->urlTemplate, $this->pageCount, $this->pageCount) . '</li>';
	        } elseif ($this->pageCount - $this->maxPages > $this->page && $this->page > $this->maxPages) {
	            $str .= '<li>' . sprintf($this->urlTemplate, 1, 1) . '</li>';
	            $str .= '<li>' . sprintf($this->urlTemplate, 2, 2) . '</li>';
	            $str .= '<li class="disabled"><a href="javascript:void(0)">...</a></li>';
	            
	            $bounds = floor($this->maxPages / 2);
	            for ($i = $this->page - $bounds; $i <= $this->page + $bounds; $i++) {
	                $str .= '<li' . ($this->page == $i ? ' class="active"' : '') . '>' . sprintf($this->urlTemplate, $i, $i) . '</li>';
	            }
	            
	            $str .= '<li class="disabled"><a href="javascript:void(0)">...</a></li>';
	            $str .= '<li>' . sprintf($this->urlTemplate, $this->pageCount - 1, $this->pageCount - 1) . '</li>';
	            $str .= '<li>' . sprintf($this->urlTemplate, $this->pageCount, $this->pageCount) . '</li>';
	        } else {
	            $str .= '<li>' . sprintf($this->urlTemplate, 1, 1) . '</li>';
	            $str .= '<li>' . sprintf($this->urlTemplate, 2, 2) . '</li>';
	            $str .= '<li class="disabled"><a href="javascript:void(0)">...</a></li>';
	            
	            for ($i = $this->pageCount - (2 + $this->maxPages); $i <= $this->pageCount; $i++) {
	                $str .= '<li' . ($this->page == $i ? ' class="active"' : '') . '>' . sprintf($this->urlTemplate, $i, $i) . '</li>';
	            }
	        }
	    } else {
            for($i = 1; $i <= $this->pageCount; $i++) {
                $str .= '<li' . ($this->page == $i ? ' class="active"' : '') . '>' . sprintf($this->urlTemplate, $i, $i) . '</li>';
            }
	    }
	    $str .= '<li' . ($this->page == $this->pageCount ? ' class="disabled"' : '') .'>' . sprintf($this->urlTemplate, $this->page+1, '&raquo;') . '</li>';
        $str .= '</ul>';
        return $str;
	}
	
	public function __toString() {
	    return $this->getHtml();
	}	
}