<?php
namespace Insane\ScriptLoader\Collection;

class CssCollection extends AbstractCollection {
    public function html() {
        $filename = $this->minify();
        return '<link rel="stylesheet" type="text/css" href="' . $this->options['cacheUrl'] . '/' . $filename . '" />';
    }
}