<?php
namespace Insane\ScriptLoader\Collection;

abstract class AbstractCollection implements CollectionInterface {
    protected $files = array();
    protected $options;
	
	public function __construct($options) {
	    $this->options = $options;
	}
	
    public function add($file, $append = true) {
		// @todo Måske skal vi tjekke om filen er tilføjet denne gruppe, men vi gør det allerede i selve script loaderen
		
        if($append) {
            $this->files[] = $file;
        } else {
            array_unshift($this->files, $file);
        }
		
		return $this;
	}
	
	protected function minify() {
	    if(empty($this->files)) {
	        return false;
	    }
	    
		$lastModified = array();

		foreach($this->files as $f) {
			$lastModified[] = filemtime($f);
		}
		$extension = pathinfo($f, PATHINFO_EXTENSION);

		rsort($lastModified);

		$filename = md5(join('', $this->files)) . '_' . $lastModified[0] . '.' . $extension;

		if(!file_exists($this->options['cachePath'] . DIRECTORY_SEPARATOR . $filename)) {
			$content = '';
			foreach($this->files as $f) {
				$output = array();
				$minimizerCmd = escapeshellcmd(sprintf('java -jar ' . dirname(__FILE__) . '/../bin/yuicompressor.jar --type %2$s --charset utf-8 %1$s', $f, $extension));
				exec($minimizerCmd, $output, $ret);
				if($ret != 0) {
				    /**
				     * @todo Do something here... For example write to the log or create a debug parameter in the script loader
				    throw new Exception('An error occurred running the minimizer. Command: ' . $minimizerCmd);
				    */
				    
				    $content .= file_get_contents($f) . "\n";
				} else {
					foreach($output as $line) {
						$content .= $line . "\n";
					}
				}
			}
			file_put_contents($this->options['cachePath'] . DIRECTORY_SEPARATOR . $filename, $content);
		}

		return $filename;
	}
	
	public function html(){}
	
	public function hasFile($file) {
	    foreach ($this->files as $f) {
	        if($f == $file) {
	            return true;
	        }
	    }
	    
	    return false;
	}
}