<?php
namespace Insane\ScriptLoader\Collection;

class JsCollection extends AbstractCollection {
    public function html() {
        $filename = $this->minify();
        return '<script type="text/javascript" src="' . $this->options['cacheUrl'] . '/' . $filename . '"></script>';
    }
}