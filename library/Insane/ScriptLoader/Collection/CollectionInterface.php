<?php
namespace Insane\ScriptLoader\Collection;

interface CollectionInterface {
    public function add($file, $append = true);
    public function html();
}