<?php
namespace Insane\ScriptLoader;

use Insane\ScriptLoader\Collection\CssCollection;
use Insane\ScriptLoader\Collection\JsCollection;
use Insane\ScriptLoader\Collection;

class ScriptLoader {
    /**
     * @var \Insane\ScriptLoader\Collection\Collection[]
     */
    protected $collections = array();
    
	protected $options;

	public function __construct($options) {
	    if(!isset($options['sourcePaths']) || !is_array($options['sourcePaths'])) {
	        throw new \InvalidArgumentException('Exception: No source paths set.');
	    }
	    if(!isset($options['cachePath']) || !is_dir($options['cachePath']) || !is_writable($options['cachePath'])) {
	        throw new \InvalidArgumentException('Exception: Cache path is not set, not a directory or is not writable.');
	    }
	    if(!isset($options['cacheUrl'])) {
	        throw new \InvalidArgumentException('Exception: Cache URL is not set.');
	    }
	    
	    $this->options = $options;
	}

	public function add($file, $append = true, $collectionName = 'default') {
	    if(!file_exists($file)) {
	        $found = false;
	        foreach ($this->options['sourcePaths'] as $path) {
	            if(file_exists($path . DIRECTORY_SEPARATOR . $file)) {
	                $file = $path . DIRECTORY_SEPARATOR . $file;
	                $found = true;
	                break;
	            }
	        }
	        
	        if(!$found) {
                throw new \InvalidArgumentException("File: $file does not exist in any of the source directories.");
	        }
	    }
	    
	    foreach ($this->collections as $name => $coll) {
	        if($coll->hasFile($file)) {
	            throw new \InvalidArgumentException("File $file already added to collection $name");
	        }
	    }
	    
	    $extension = pathinfo($file, PATHINFO_EXTENSION);
	    $collectionName = $collectionName . $extension;
	    
	    if(!isset($this->collections[$collectionName])) {
    	    switch ($extension) {
    	        case 'js':
    	            $this->collections[$collectionName] = new JsCollection($this->options);
    	            break;
    	        case 'css':
    	            $this->collections[$collectionName] = new CssCollection($this->options);
    	            break;
	            default:
	                throw new \InvalidArgumentException("The type of file $file could not be determined.");
	                break;
    	    }
	    }
	    
	    $this->collections[$collectionName]->add($file, $append);
	    
	    return $this;
	}
	
	public function htmlCss() {
	    $str = '';
	    foreach ($this->collections as $collection) {
	        if(!$collection instanceof CssCollection) {
	            continue;
	        }
	        $str .= $collection->html() . "\n";
	    }
	    return $str;
	}
	
    public function htmlJs() {
	    $str = '';
	    foreach ($this->collections as $collection) {
	        if(!$collection instanceof JsCollection) {
	            continue;
	        }
	        $str .= $collection->html() . "\n";
	    }
	    return $str;
	}
}