<?php
/**
 * @deprecated Use Zend\Mvc\Controller\Plugin\FlashMessenger instead
 */
namespace Insane\FlashMessenger;

class FlashMessenger {
	protected $messages = array();
	
	const TYPE_SUCCESS = 'success';
	const TYPE_MESSAGE = 'message';
	const TYPE_WARNING = 'warning';
	const TYPE_ERROR = 'error';
	
	public function add($message, $type = 'message') {
	    if(!isset($this->messages[$type])) {
	        $this->messages[$type] = array();
	    }
	    
        $this->messages[$type][] = $message;
	}

	public function getMessages() {
		return $this->messages;
	}

	public function hasMessages() {
	    // @todo This method should probably count the individual types of messages and return a status based on that
		return count($this->messages) > 0;
	}

	public function reset() {
		$this->messages = array();
	}
	
	public function getHtml() {
	    if(empty($this->messages)) {
	        return '';
	    }
	    
	    $str = '';
	    foreach ($this->messages as $type => $messages) {
    	    $str .= '<div class="flashmessenger flashmessenger-' . $type . '">';
        	    $str .= '<ul>';
        	    foreach ($messages as $message) {
        	        $str .= '<li>' . $message . '</li>';
        	    }
        	    $str .= '</ul>';
    	    $str .= '</div>' . "\n";
	    }
	    
        $this->reset();
        
        return $str;
	}
	
	public function __toString() {
	    return $this->getHtml();
	}	
}